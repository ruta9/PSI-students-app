package app.com.mifapp.schedule;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import app.com.mifapp.R;

public class WeekDay extends AppCompatActivity {

    private Calendar calendar = Calendar.getInstance();
    private int day = calendar.get(Calendar.DAY_OF_WEEK);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private String date = sdf.format(new Date());
    private Date start = new Date();
    private TextView[] textViews= new TextView[20];
    public static String n;
    private int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekday);
        setTextViews();
        String method = this.getIntent().getStringExtra("day");
        if(method != null) {
            switch(method){
                case "monday" :monday();
                    break;
                case "tuesday": tuesday();
                    break;
                case "wednesday": date = "2017-10-04";
                    wednesday();
                    break;
                case "thursday": thursday();
                    break;
                case "friday": friday();
                    break;
                case "wednesday2": date = "2017-10-11";
                    wednesday();
                    break;
            }
        }
        else{
            textViews[19].setText(date);
            if(n==null || this.getIntent().getStringExtra("Tag") !=null)n = this.getIntent().getStringExtra("Tag");
            checkHours();
            switch (day){
                case Calendar.MONDAY: monday();
                    break;
                case Calendar.TUESDAY: tuesday();
                    break;
                case Calendar.WEDNESDAY: wednesday();
                    break;
                case Calendar.THURSDAY: thursday();
                    break;
                case Calendar.FRIDAY: friday();
                    break;
                case Calendar.SATURDAY: textViews[18].setText(getString(R.string.sestadienis));
                    break;
                case Calendar.SUNDAY: textViews[18].setText(getString(R.string.sekmadienis));
                    break;
            }
        }



    }

    public void setTextViews(){
        textViews[0] = (TextView)findViewById(R.id.lentele1_1);
        textViews[1] = (TextView)findViewById(R.id.lentele1_2);
        textViews[2] = (TextView)findViewById(R.id.lentele1_3);
        textViews[3] = (TextView)findViewById(R.id.lentele2_1);
        textViews[4] = (TextView)findViewById(R.id.lentele2_2);
        textViews[5] = (TextView)findViewById(R.id.lentele2_3);
        textViews[6] = (TextView)findViewById(R.id.lentele3_1);
        textViews[7] = (TextView)findViewById(R.id.lentele3_2);
        textViews[8] = (TextView)findViewById(R.id.lentele3_3);
        textViews[9] = (TextView)findViewById(R.id.lentele4_1);
        textViews[10] = (TextView)findViewById(R.id.lentele4_2);
        textViews[11] = (TextView)findViewById(R.id.lentele4_3);
        textViews[12] = (TextView)findViewById(R.id.lentele5_1);
        textViews[13] = (TextView)findViewById(R.id.lentele5_2);
        textViews[14] = (TextView)findViewById(R.id.lentele5_3);
        textViews[15] = (TextView)findViewById(R.id.lentele6_1);
        textViews[16] = (TextView)findViewById(R.id.lentele6_2);
        textViews[17] = (TextView)findViewById(R.id.lentele6_3);
        textViews[18] = (TextView)findViewById(R.id.lentele_diena);
        textViews[19] = (TextView)findViewById(R.id.lentele_data);
    }

    public void monday(){
        textViews[18].setText(getString(R.string.pirmadienis));
        if(checkHolidays()){
            textViews[7].setText("Programų sistemų inžinerija I/II d.\n(PASKAITA)\nKarolis Petrauskas, Doc., Dr.");
            textViews[10].setText("Programų sistemų inžinerija I/II d.\n(PRATYBOS)\nKarolis Petrauskas, Doc., Dr.");
            textViews[10].setTextColor(Color.parseColor("#c11c1c"));
            textViews[8].setText("103\n(MIF-Didl.)");
            textViews[11].setText("303\n(MIF-Didl.)");
            textViews[9].setTextColor(Color.parseColor("#c11c1c"));
            textViews[11].setTextColor(Color.parseColor("#c11c1c"));
            switch(n) {
                case "1":
                    textViews[13].setText("Taikomasis objektinis\n programavimas(PRATYBOS)\nKarolis Uosis, Lekt.");
                    textViews[14].setText("317\n(MIF-Didl.)");
                    textViews[12].setTextColor(Color.parseColor("#c11c1c"));
                    textViews[13].setTextColor(Color.parseColor("#c11c1c"));
                    textViews[14].setTextColor(Color.parseColor("#c11c1c"));
                    break;
            }
        }

    }

    public void tuesday(){
        textViews[18].setText(getString(R.string.antradienis));
        if(checkHolidays()){
            textViews[8].setText("413\n(MIF-Didl.)");
            textViews[8].setTextColor(Color.parseColor("#c11c1c"));
            textViews[6].setTextColor(Color.parseColor("#c11c1c"));
            textViews[11].setText("103\n(MIF-Didl.)");
            textViews[14].setText("103\n(MIF-Didl.)");
            textViews[7].setText("Matematinė logika\n(PRATYBOS)\nJulius Andrikonis, Asist., Dr.");
            textViews[7].setTextColor(Color.parseColor("#c11c1c"));
            textViews[10].setText("Matematinė logika\n(PASKAITA)\nJulius Andrikonis, Asist., Dr.");
            textViews[13].setText("Taikomasis objektinis\nprogramavimas(PASKAITA)\nVytautas Ašeris, Partn. doc., Dr.");
        }

    }

    public void wednesday(){
        textViews[18].setText(getString(R.string.treciadienis));
        if(checkHolidays()){
            if(date.equals("2017-10-04")||date.equals("2017-10-18")||date.equals("2017-11-15")||date.equals("2017-11-29")||date.equals("2017-12-13")){
                textViews[7].setText("Programų sistemų inžinerija I/II d.\n(PRATYBOS)\nKarolis Petrauskas, Doc., Dr.");
                textViews[8].setText("303\n(MIF-Didl.)");
                textViews[6].setTextColor(Color.parseColor("#c11c1c"));
                textViews[7].setTextColor(Color.parseColor("#c11c1c"));
                textViews[8].setTextColor(Color.parseColor("#c11c1c"));
            }
            else{
                textViews[7].setText("Programų sistemų inžinerija I/II d.\n(PASKAITA)\nKarolis Petrauskas, Doc., Dr.");
                textViews[8].setText("103\n(MIF-Didl.)");
            }
            textViews[10].setText("Duomenų bazių valdymo sistemos\n(PASKAITA)\nRomas Baronas, Prof., Dr. (HP)");
            textViews[11].setText("103\n(MIF-Didl.)");
            textViews[12].setTextColor(Color.parseColor("#c11c1c"));
            textViews[13].setTextColor(Color.parseColor("#c11c1c"));
            textViews[14].setTextColor(Color.parseColor("#c11c1c"));
            switch(n) {
                case "1":textViews[13].setText("Duomenų bazių valdymo sistemos\n(PRATYBOS)\nLina Povilavičiūtė, Asist.");
                    textViews[14].setText("318\n(MIF-Didl.)");
                    break;
                case "2":textViews[13].setText("Taikomasis objektinis\n programavimas(PRATYBOS)\nAurimas Šimkus, Lekt.");
                    textViews[14].setText("319\n(MIF-Didl.)");
                    textViews[16].setText("Duomenų bazių valdymo sistemos\n(PRATYBOS)\nLina Povilavičiūtė, Asist.");
                    textViews[17].setText("318\n(MIF-Didl.)");
                    textViews[15].setTextColor(Color.parseColor("#c11c1c"));
                    textViews[16].setTextColor(Color.parseColor("#c11c1c"));
                    textViews[17].setTextColor(Color.parseColor("#c11c1c"));
                    break;
            }
        }

    }

    public void thursday(){
        textViews[18].setText(getString(R.string.ketvirtadienis));
        busas();
    }

    public void friday(){
        textViews[18].setText(getString(R.string.penktadienis));
        busas();
    }

    public void busas(){
        if(checkHolidays()){
            textViews[10].setPaintFlags( textViews[10].getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            textViews[10].setText("Pasirinktas\n BUS'as");
            textViews[10].setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://is.vu.lt/pls/pub/vustud.public_ni$wwwbul.dalsar_show"));
                    startActivity(intent);
                }
            });
        }
    }
    public void AllDays(View view) {
        Intent intent = new Intent(this, AllDays.class);
        startActivity(intent);
    }

    public boolean checkHolidays(){
        try {
            Date end = sdf.parse("2017-12-24");
            if(start.after(end) || date.equals("2017-11-01"))return false;
            else return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public void checkHours(){
        if(hour >= 8 && hour <10) {
            textViews[0].setBackgroundResource(R.drawable.textview_border2);
            textViews[1].setBackgroundResource(R.drawable.textview_border2);
            textViews[1].setTypeface(null, Typeface.BOLD);
            textViews[2].setBackgroundResource(R.drawable.textview_border2);
        }
        else if (hour >= 10 && hour <12){
            textViews[3].setBackgroundResource(R.drawable.textview_border2);
            textViews[4].setBackgroundResource(R.drawable.textview_border2);
            textViews[4].setTypeface(null, Typeface.BOLD);
            textViews[5].setBackgroundResource(R.drawable.textview_border2);
        }
            else if(hour >= 12 && hour <14){
            textViews[6].setBackgroundResource(R.drawable.textview_border2);
            textViews[7].setBackgroundResource(R.drawable.textview_border2);
            textViews[7].setTypeface(null, Typeface.BOLD);
            textViews[8].setBackgroundResource(R.drawable.textview_border2);
            }
                else if(hour >= 14 && hour <16){
                    textViews[9].setBackgroundResource(R.drawable.textview_border2);
                    textViews[10].setBackgroundResource(R.drawable.textview_border2);
                    textViews[10].setTypeface(null, Typeface.BOLD);
                    textViews[11].setBackgroundResource(R.drawable.textview_border2);
                }
                    else if(hour >= 16 && hour <18){
                        textViews[12].setBackgroundResource(R.drawable.textview_border2);
                        textViews[13].setBackgroundResource(R.drawable.textview_border2);
                        textViews[13].setTypeface(null, Typeface.BOLD);
                        textViews[14].setBackgroundResource(R.drawable.textview_border2);
                    }
                        else if(hour >= 18 && hour <20){
                            textViews[15].setBackgroundResource(R.drawable.textview_border2);
                            textViews[16].setBackgroundResource(R.drawable.textview_border2);
                            textViews[16].setTypeface(null, Typeface.BOLD);
                            textViews[17].setBackgroundResource(R.drawable.textview_border2);
                        }
    }
}
