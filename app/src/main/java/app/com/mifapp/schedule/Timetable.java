package app.com.mifapp.schedule;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import app.com.mifapp.R;

public class Timetable extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);
    }

    public void EnterTimetable(View view) {
        Intent intent = new Intent(this, WeekDay.class);
        intent.putExtra("Tag", "" + view.getTag());
        startActivity(intent);
    }
}
