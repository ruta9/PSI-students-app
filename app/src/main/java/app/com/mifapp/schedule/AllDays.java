package app.com.mifapp.schedule;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import app.com.mifapp.R;

public class AllDays extends AppCompatActivity {
    private String pasirinkimas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_days);
        TextView textViews1 = (TextView)findViewById(R.id.textView2);
        TextView textViews2 = (TextView)findViewById(R.id.textView3);
        textViews1.setPaintFlags( textViews1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textViews2.setPaintFlags( textViews2.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }
    public void enterTimeTable (View view){
        pasirinkimas = view.getTag().toString();
        doActions();
    }

    public void doActions(){
        Intent intent = new Intent(this, WeekDay.class);
        switch(pasirinkimas){
            case "1": intent.putExtra("day","monday");
                startActivity(intent);
                break;
            case "2": intent.putExtra("day","tuesday");
                startActivity(intent);
                break;
            case "3":intent.putExtra("day","wednesday");
                startActivity(intent);
                break;
            case "7":intent.putExtra("day","wednesday2");
                startActivity(intent);
                break;
            case "4":intent.putExtra("day","thursday");
                startActivity(intent);
                break;
            case "5":intent.putExtra("day","friday");
                startActivity(intent);
                break;
            case "6":Intent intent2 = new Intent(this, Weeks.class);
                startActivity(intent2);
                break;
        }
    }
}
