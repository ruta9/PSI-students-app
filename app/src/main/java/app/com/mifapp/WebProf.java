package app.com.mifapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebProf extends AppCompatActivity {

    private String url="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_prof);

        String n = this.getIntent().getStringExtra("Tag");
        switch (n){
            case "1": setUrl(getString(R.string.K_Petrauskas)); break;
            case "2": setUrl(getString(R.string.J_Andrikonis)); break;
            case "3": setUrl(getString(R.string.R_Baronas)); break;
            case "4": setUrl(getString(R.string.V_Aseris)); break;
            case "5": setUrl(getString(R.string.K_Uosis)); break;
        }

        WebView webView = (WebView) findViewById(R.id.Window1);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);
    }
    public void setUrl(String url){
        this.url = url;
    }

    @Override
    public void onBackPressed() {
        if (((WebView) findViewById(R.id.Window1)).canGoBack()) {
            ((WebView) findViewById(R.id.Window1)).goBack();
            return;
        }
        // Otherwise defer to system default behavior.
        super.onBackPressed();
    }


}
