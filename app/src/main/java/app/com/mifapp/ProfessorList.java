package app.com.mifapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ProfessorList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professor_list);
    }

    public void EnterPage(View view){
        Intent intent = new Intent(this, WebProf.class);
        intent.putExtra("Tag", "" + view.getTag());
        startActivity(intent);
    }
}
