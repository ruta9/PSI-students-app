package app.com.mifapp.events_calendar;
import android.content.res.Resources;
import org.json.JSONArray;
import org.json.JSONException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class APIService {

    static JSONArray json;
    static String easter = "";
    static Resources res;


    static void parseHoliday() throws JSONException, IOException {
        int i;
        for(i = 0; i < json.length(); i++){
            if (json.getJSONObject(i).getJSONArray("celebrations").getJSONObject(0).getString("title").equals("Easter Sunday of the Resurrection of the Lord")){
                easter = json.getJSONObject(i).getString("date");
            }
        }
    }

    static void getJSONObjectFromURL(int year, int month, Resources resr) throws IOException, JSONException {
        res = resr;
        HttpURLConnection urlConnection = null;
        URL url = new URL("http://calapi.inadiutorium.cz/api/v0/en/calendars/default/"+year+"/"+month);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000 /* milliseconds */ );
        urlConnection.setConnectTimeout(15000 /* milliseconds */ );
        urlConnection.setDoOutput(true);
        urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        String jsonString = sb.toString();
        json = new JSONArray(jsonString);
        parseHoliday();
    }
}
