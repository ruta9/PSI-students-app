package app.com.mifapp.events_calendar;


import android.content.res.Resources;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static app.com.mifapp.events_calendar.APIService.getJSONObjectFromURL;

public class Holidays {

    static String[] holidayDates = {"1-1", "2-16", "3-11", "5-1",
                                    "6-24", "7-6", "8-15", "11-1",
                                    "12-24", "12-25", "12-26"};
    static String[] holidayInfo = {"Naujieji metai", "Lietuvos valstybės atkūrimo diena",
                                    "Lietuvos nepriklausomybės atkūrimo diena", "Tarptautinė darbo diena",
                                    "Joninės", "Lietuvos karaliaus Mindaugo karūnavimo diena",
                                    "Žolinė", "Visų Šventųjų diena", "Kūčios", "Kalėdos, 1 diena", "Kalėdos, 2 diena"};

static Map<Integer, String> getHolidaysOfTheMonth(int year, int month, Resources res) throws IOException, JSONException {
    month++;

    Map<Integer, String> days = new HashMap<Integer, String>();
    for(String holiday : holidayDates){
        if (Integer.parseInt(holiday.split("-")[0]) == month)
            days.put(Integer.parseInt(holiday.split("-")[1]),  Arrays.asList(holidayInfo).get(Arrays.asList(holidayDates).indexOf(holiday)));
    }
    if (month == 3)
        getJSONObjectFromURL(year, 3, res);
    else if (month == 4)
        getJSONObjectFromURL(year, 4, res);
    else APIService.easter = "";
    if (!APIService.easter.equals("")){
        days.put(Integer.parseInt(APIService.easter.split("-")[2]), "Vėlykos, pirma diena");
        days.put(Integer.parseInt(APIService.easter.split("-")[2])+1, "Vėlykos, antra diena");
    }
    return days;
}


}
