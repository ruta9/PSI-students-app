package app.com.mifapp.events_calendar;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import app.com.mifapp.R;


public class Calendar extends AppCompatActivity {

    private GregorianCalendar today = new GregorianCalendar();
    private GregorianCalendar showing = new GregorianCalendar();
    private String[] months = {"Sausis",
            "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"};
    private String[] Weekdays = {"Pr", "An", "Tr", "Kt", "Pn", "Š", "S"};
    private GridLayout mainLayout;
    private ConstraintLayout showingMonth;
    private ConstraintLayout about;
    private Map<Integer, String> holidays = new HashMap<Integer, String>();
    boolean holiday_found = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        showing=new GregorianCalendar();

        //Prepare layout for TextView adding
        mainLayout = (GridLayout) findViewById(R.id.days);
        GridLayout gl = (GridLayout) findViewById(R.id.weekdays);
        mainLayout.setColumnCount(7);
        gl.setColumnCount(7);
        showingMonth = (ConstraintLayout) findViewById(R.id.main);
        about = (ConstraintLayout) findViewById(R.id.about);

        //Print the labels of the weekdays
        for(String day : Weekdays){
            TextView textview = CreateView(day, 20, 100, 100, day, "#BDBDBD");
            textview.setGravity(Gravity.CENTER);
            gl.addView(textview);
        }
        //Get this months' 1 day, change calendar label and create the calendar view
        showing.set(today.get(GregorianCalendar.YEAR),today.get(GregorianCalendar.MONTH),1);
        try {
            CreateMonthView(showing, mainLayout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setMonthLabel(showingMonth, showing.get(GregorianCalendar.MONTH));

    }

    // Next month
    public void Next(View view) throws InterruptedException {
        holidays.clear();
        showing.add(GregorianCalendar.MONTH, 1);
        mainLayout.removeAllViewsInLayout();
        CreateMonthView(showing, mainLayout);
        setMonthLabel(showingMonth, showing.get(GregorianCalendar.MONTH));

    }
    // Go back a month
    public void Back (View view) throws InterruptedException {
        holidays.clear();
        showing.add(GregorianCalendar.MONTH, -1);
        mainLayout.removeAllViewsInLayout();
        CreateMonthView(showing, mainLayout);
        setMonthLabel(showingMonth, showing.get(GregorianCalendar.MONTH));
    }

    private TextView CreateView(String tag, int textsize, int width, int height, String text, String color){
        TextView textfield = new TextView(this);
        textfield.setTag(tag);
        textfield.setTextSize(textsize);
        textfield.setWidth(width);
        textfield.setHeight(height);
        textfield.setText(text);
        textfield.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                //Look for a holiday that would be on the clicked day of the month
                for(int day:holidays.keySet()) {
                    if (v.getTag().equals(day+"")) {

                        holiday_found = true;
                        for (int i = 0; i < about.getChildCount(); i++) {
                            if (about.getChildAt(i).getTag().equals("AboutHoliday"))
                                ((TextView) about.getChildAt(i)).setText(holidays.get(day));
                        }

                    }
                }
                //Holiday not found - print message with that info
                if (!holiday_found) {
                    for (int i = 0; i < about.getChildCount(); i++) {
                        if (about.getChildAt(i).getTag().equals("AboutHoliday"))
                            ((TextView) about.getChildAt(i)).setText("Šiandien jokie renginiai nevyksta.");
                    }
                }
            }
        });
        textfield.setTextColor((Color.parseColor(color)));
        return textfield;
    }

    private void CreateMonthView(GregorianCalendar cal, GridLayout tl) throws InterruptedException {
        holiday_found=false;
        ((TextView)findViewById(R.id.AboutHoliday)).setText("");
        int i = 1;
        int diena = 1;
        int FirstDayOfTheMonth = cal.get(GregorianCalendar.DAY_OF_WEEK);
        FirstDayOfTheMonth--;
        if(FirstDayOfTheMonth == 0) FirstDayOfTheMonth = 7;

        while( i >= 1 && i <= cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)) {
            if (diena >= FirstDayOfTheMonth) {
                TextView textview = CreateView(Integer.toString(i), 20, 100, 100, "" + i, "#000000");
                textview.setGravity(Gravity.CENTER);

                //If today's date is seen, circle it
                if (i == this.today.get(GregorianCalendar.DAY_OF_MONTH) && cal.get(GregorianCalendar.MONTH) == today.get(GregorianCalendar.MONTH ) && cal.get(GregorianCalendar.YEAR) == today.get(GregorianCalendar.YEAR ))
                    textview.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle, null));

                tl.addView(textview);
                i++;
            }

            //if month does not start from monday
            else {
                diena++;
                TextView textview = CreateView("blank", 20, 65, 65, "   ", "#000000");
                tl.addView(textview);
            }
        }
        //check for holidays in this month
        checkHolidays();
    }

    private void setMonthLabel(ConstraintLayout l, int month){
        for(int j = 0; j < l.getChildCount(); j++ )
            if( l.getChildAt(j) instanceof TextView && l.getChildAt(j).getTag().equals("month"))
                ((TextView) l.getChildAt(j)).setText(this.months[month]);
    }

    private void downloadHolidays() throws InterruptedException {
        //----------------
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                try {
                    holidays = Holidays.getHolidaysOfTheMonth(showing.get(GregorianCalendar.YEAR), showing.get(GregorianCalendar.MONTH), getResources());
                }  catch (JSONException e) {
                    e.printStackTrace();
                }  catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        thread.join();
        //----------------------
    }

    private void checkHolidays() throws InterruptedException {
        downloadHolidays();
        if (holidays.size() != 0){
            for(int day : holidays.keySet()){
                ((TextView)mainLayout.findViewWithTag(day+"")).setTextColor(Color.parseColor("#FF0000"));
            }
        }

    }

}
