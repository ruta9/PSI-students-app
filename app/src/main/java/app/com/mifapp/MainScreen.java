package app.com.mifapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import app.com.mifapp.events_calendar.Calendar;
import app.com.mifapp.schedule.Timetable;

public class MainScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
    }


    public void ClickProfessors(View view){
        Intent intent = new Intent(this, ProfessorList.class);
        startActivity(intent);
    }

    public void ClickTimetable(View view){
        Intent intent = new Intent(this, Timetable.class);
        startActivity(intent);
    }

    public void ClickEvents(View view){
        Intent intent = new Intent(this, Calendar.class);
        startActivity(intent);
    }


    public void ClickLiterature(View view){
        Intent intent = new Intent(this, StudentNotes.class);
        startActivity(intent);
    }



    public void ClickDeadlines(View view){
        Intent intent = new Intent(this, Deadlines.class);
        startActivity(intent);
    }
}
