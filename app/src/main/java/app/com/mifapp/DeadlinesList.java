package app.com.mifapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DeadlinesList extends AppCompatActivity {
    private TextView txtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deadlines_list);

        txtView=new TextView(this);
        txtView=(TextView)findViewById(R.id.txtView);
        String n = this.getIntent().getStringExtra("Tag");
        switch (n){
            case "1":  txtView.setText( "Iki spalio 15d. - 1 lab. darbas\n\n" +
                                        "Iki lapkričio 12d. - 2 lab.darbas\n\n" +
                                        "Iki gruodžio 23d. - 3 lab. darbas\n\n"); break;
            case "2":  txtView.setText("Šiuo metu nėra artėjančių atsiskaitymų\n\n"); break;
            case "3":  txtView.setText( "1 lab. darbas: \n\n" +
                                        ".Iki spalio 14d. 1-a dalis\n(1-a ir 2-a užklausos)\n" +
                                        ".Iki spalio 28d. 2-a dalis\n(3-a ir 4-a užklausos)\n" +
                                        ".Iki lapkričio 11d. 3-a dalis\n(5-a užklausa)\n\n" +
                                        "2 lab. darbas: \n\n" +
                                        ".Iki lapkričio 25d. 1-a dalis\n" +
                                        ".Iki gruodžio 9d. 2-a dalis\n" +
                                        ".Iki gruodžio 23d. 3-a dalis\n"); break;
            case "4":  txtView.setText( "Iki 7-os savaitės - 1 lab. darbas\n\n" +
                                        "Iki 10-os savaitės - 2 lab.darbas\n\n" +
                                        "Iki 14-os savaitės - 3 lab. darbas\n\n"); break;

        }


    }

}
