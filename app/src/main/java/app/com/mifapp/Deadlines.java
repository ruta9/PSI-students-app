package app.com.mifapp;


import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Deadlines extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deadlines);
        }

    public void EnterDeadlinesList(View view){
        Intent intent = new Intent(this, DeadlinesList.class);
        intent.putExtra("Tag", "" + view.getTag());
        startActivity(intent);
    }
}

